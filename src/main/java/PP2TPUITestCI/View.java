package PP2TPUITestCI;
import PP2TPCoreTestCI.*;

public class View implements PP2TPCoreTestCI.Observer {
	Model myModel;
	Controller myController;
	
	
	public View (Model m) {
		myModel = m;
		myModel.attach(this);
		
	}
	
	public void detachView() {
		myModel.detach(this);
	}
	
	public void initialize() {
		myController = makeController();
		this.dibujar();
	}
	public Controller makeController() {
		return new Controller(this);
	}
	
	public void dibujar() {
		for (int i=0; i<myModel.getParties().size(); i++) {
			System.out.println("The party: "
							+ myModel.getParties().get(i)
							+" -has- "
							+myModel.getVotes().get(i)
							+" votes.");
		}
	}
	
	@Override
	public void update() {
		this.dibujar();		
	}
	
	public Model getModel() {
		return myModel;
	}
	public Controller getController() {
		return myController;
	}
}
