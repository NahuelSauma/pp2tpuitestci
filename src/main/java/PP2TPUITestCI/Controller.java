package PP2TPUITestCI;
import PP2TPCoreTestCI.*;

public class Controller implements PP2TPCoreTestCI.Observer{
	View myView;
	Model myModel;
	
	public Controller (View v) {
		myView = v;
		myModel = myView.getModel();
		myModel.attach(this);
	}
	
	
	
	public void detachController() {
		myModel.detach(this);
	}
	
	@Override
	public void update() {
		
	}

}
