package PP2TPUITestCI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import PP2TPCoreTestCI.*;

public class MainDePrueba {

	public static void main(String[] args) {
		
		List<String> parties = new ArrayList<String>();
		List<Long> votes = new ArrayList<Long>();
		parties.add("negro");
		votes.add(1L);
		parties.add("rojo");
		votes.add(1L);
		parties.add("azul");
		votes.add(1L);
		parties.add("blanco");
		votes.add(1L);
		parties.add("amarillo");
		votes.add(1L);
		
		Model m = new Model(parties, votes);
		View v = new View(m);
		v.initialize();
		int i=0;
		while (i<10) {
			m.changeVote();
			i++;
			try {
				TimeUnit.SECONDS.sleep(1);				
			}catch(InterruptedException ex) {
				System.out.println(ex);
			}
		}
	}

}
